# coding: utf-8

import pygame as pg
import random
import string
import client
import time

#TODO
'''se connecter au serveur
on recoit le num d'utilisateur

fonction jeu{
    
}
'''

pg.init()

#ecran = pg.display.set_mode((300, 200))
myClock= pg.time.Clock()
continuer = True
nbCoups=10
string.letters= 'abcdefghijklmnopqrstuvwxyz'

host = "192.168.1.52"#socket.gethostname()                           
port = 9999
idclient= 'O'
timeMax=5000

def gameInit():
    global Cl
    global idclient
    Cl = client.Client(host,port)
    Cl.send({
        "event":"connexion"
    })
    idclient = Cl.receive()['response']
    print(idclient)
    return idclient


def TempsDeReponse(key,timeMax):
    PassedTime=0
    continuer=True
    while continuer:
        myClock.tick()
        for event in pg.event.get():
            if event.type == pg.KEYDOWN:
                if PassedTime>timeMax or key==pg.key.name(event.key):
                    continuer=False
                print(pg.key.name(event.key))
        if PassedTime>timeMax:
            return PassedTime
        PassedTime+=myClock.get_time()
    return PassedTime

def CharEnvoye(timeMax):
    PassedTime=0
    continuer=True
    while continuer:
        myClock.tick()
        for event in pg.event.get():
            if event.type == pg.KEYDOWN:
                
                return pg.key.name(event.key)
                print(pg.key.name(event.key))
                print(myClock.get_time())
        PassedTime+=myClock.get_time()
        if PassedTime>timeMax :
            return random.choice(string.letters)

def launchWindow(title):
    screen = pg.display.set_mode((640, 280))
    pg.display.set_caption(title)
    score = 0

    draw(screen,"préparez vous a jouer")
    if type(gameInit())!='NonType':
        draw(screen,"vous etes dans la partie")
    else:
        draw(screen,"erreur de connexion!")
    
    for i in range (nbCoups):
        score+=scoreDuel(screen)
    
    print("votre score total est")
    print(score)

    draw(screen,"le jeu est fini " + str(score))
    Cl.shutdown()
    

    while True:       
        for event in pg.event.get():
            # exit conditions --> windows titlebar x click
            if event.type == pg.QUIT:
                    pg.quit()
                    return


def scoreDuel(screen):
    score=0
    draw(screen,"tapez une lettre pour challenger votre adversaire")
    chartosend = CharEnvoye(3000)
    Cl.send({
        'event': 'sendchar',
        'char': chartosend,
        'id': idclient
    })
    draw(screen,"en attente de votre adversaire")

    Cl.send({
       'event': 'askchar',
       'id': idclient
    })
    
    lettreRecue = None
    while lettreRecue == None:
        lettreRecue = Cl.receive()
        time.sleep(.200)
    lettreRecue = lettreRecue['response']
        
    draw(screen,"tapez vite "+lettreRecue)

    int(score)
    
    score+=(timeMax/TempsDeReponse(lettreRecue,timeMax))
    print(score)
    return score


def draw(screen,string):
    # use a (r, g, b) tuple for color
    yellow = (255, 255, 0)
    black=(0,0,0)
    screen.fill(black)
    # pick a font you have and set its size
    myfont = pg.font.SysFont("Comic Sans MS", 30)
    # apply it to text on a label
    label = myfont.render(string, 1, yellow)
    # put the label object on the screen at point x=100, y=100
    screen.blit(label, (100, 100))
    # show the whole thing
    pg.display.flip()

#c=CharEnvoye(5000)
#t=TempsDeReponse('k',5000)
#print(t)
#draw("hello")
launchWindow("titre")
#gameInit()

#pg.quit()