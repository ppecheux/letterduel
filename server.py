import socket 
import selectors 
import sys
import threading   
import pickle                             


#class Server():


# create a socket object
serversocket = socket.socket(
	        socket.AF_INET, socket.SOCK_STREAM) 

# get local machine name
host = socket.gethostname()                           

port = 9999                                           

# bind to the port
serversocket.bind((host, port))                                  

#server start listenning
serversocket.listen(10)

connections = []
players = 0
listechars = [None,None,None]

def response(code):
    global players
    global listechars
    data = pickle.loads(code)
    #print(data)

    if data["event"] == "connexion":
        players = players + 1
        return {
            "response": str(players)
        }

    if data["event"] == "askchar":
        idu = int(data['id'])
        char = listechars[idu]
        if char:
            listechars[idu] = None
            return {
                'response': char
            }
        else:
            return None
    
    if data["event"] == "sendchar":
        idu = int(data['id'])
        listechars[(idu%players) + 1] = data['char']   
        return None
    return None



def handler(c,a):
    global connections
    while True:
        data = c.recv(1024)
        data = response(data)
        if data:
            print(data)
            for connection in connections:
                connection.send(pickle.dumps(data))
        # else:
        #     connections.remove(c)
        #     c.close()
        #     break

while True:
    # establish a connection
    clientsocket,addr = serversocket.accept()
    cthread = threading.Thread(target=handler, args= (clientsocket, addr))
    cthread.daemon = True
    cthread.start()

    connections.append(clientsocket)
