import socket
import pickle

class Client():
    def __init__(self, ip, port):
        self.data = "aucune données"
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(5)
        try:
            self.sock.connect((ip,port))
        except socket.timeout:
            print('Timeout at connect')

    def send(self, message):
        try:
            self.sock.send(pickle.dumps(message))
        except socket.timeout:
            print('Timeout at send')
    def receive(self):
        try:
            self.data = pickle.loads(self.sock.recv(1024))
            print(self.data)
            return self.data
        except socket.timeout:
            print('Timeout at receive')
        

    def shutdown(self):
        self.sock.close()



# # get local machine name
# host = "192.168.1.52"#socket.gethostname()                           
# port = 9999

# message = {
#     "name":"carole",
#     "age":"13"
# }

# cl = Client(socket.gethostname(), port)
# cl.send(message)
# cl.receive()
# print(cl.data) 

